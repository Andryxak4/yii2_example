<?php
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>

<?php
$allflash = \Yii::$app->session->getAllFlashes();
?>
    <div class="row">
        <div class="col-xs-12">
            <?php foreach (Yii::$app->session->getAllFlashes() as $type => $message): ?>
                <?php if (in_array($type, ['success', 'danger', 'warning', 'info'])): ?>
                    <?= yii\bootstrap\Alert::widget([
                            'options' => ['class' => 'alert-dismissible alert-'.$type],
                            'body' => $message
                    ]) ?>
                <?php endif ?>
            <?php endforeach ?>
        </div>
    </div>

<div class="site-index">




    <div class="jumbotron">
        <h1>Payment!</h1>


    </div>

</div>

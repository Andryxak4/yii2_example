<?php
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Payment!</h1>

        <p class="lead">For download   you should go to PayPal.</p>

        <?php if (isset($response['redirect_url']) && !empty($response['redirect_url'])): ?>
            <p><a class="btn btn-lg btn-success" href="<?= $response['redirect_url'] ?>">Go to PayPal</a></p>
        <?php endif; ?>


    </div>


</div>

<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {

//         return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
//                    ['allow' => true, 'actions' => ['register', 'connect'], 'roles' => ['?']],
//                    ['allow' => true, 'actions' => ['payment', 'download'], 'roles' => ['?', '@']],
//                ],
//            ],
//        ];
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'payment', 'download', 'success', 'cancel'],
                'rules' => [
                    [
                        'actions' => ['logout', 'payment', 'download'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['success', 'cancel'],
                        'roles' => ['?', '@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Payment url
     *
     * @return payment url
     */
    public function actionPayment() {



        if (Yii::$app->user->identity->isPAymentConfirm()):

            Yii::$app->getResponse()->redirect(array('/download'));
        else:

            $params = [
                'currency' => 'USD', // only support currency same PayPal
                'description' => 'Buy some item',
                'total_price' => 10,
                'email' => 'nguyentruongthanh.dn@gmail.com',
                'items' => [
                    [
                        'id' => Yii::$app->user->id,
                        'name' => 'Payment for membership',
                        'quantity' => 1,
                        'price' => 10
                    ],
                ]
            ];
            $response = Yii::$app->payPalRest->getLinkCheckOut($params);

            if (isset($response['payment_id']) && !empty($response['payment_id'])):
                Yii::$app->user->identity->setPaymentId($response['payment_id']);
            endif;



            return $this->render('payment', array(
                        'response' => $response
            ));

        endif;
    }

    /* download file after payment */

    public function actionDownload() {
        if (Yii::$app->user->identity->isPAymentConfirm()):
            return $this->render('download');
        else:
            Yii::$app->getResponse()->redirect(array('/payment'));
        endif;
    }

    public function actionSuccess() {
        $request = Yii::$app->request->get();
        if (isset($request['paymentId']) && !empty($request['paymentId']) && isset($request['token']) && !empty($request['token']) && isset($request['PayerID']) && !empty($request['PayerID'])):
            $response = Yii::$app->payPalRest->getResult($request['paymentId']);

            if (isset($response) && isset($response['payer']['status']) && isset($response['payer']['status']) == 'VERIFIED'):
                Yii::$app->user->identity->confirmPayment($request['paymentId']);
                return $this->render('payment_answer');
            else:
                Yii::$app->session->setFlash('danger', \Yii::t('user', 'Payment is empty'));
                return $this->render('payment_answer');
            endif;
        endif;
    }

    public function actionCancel() {
        Yii::$app->session->setFlash('danger', \Yii::t('user', 'Your Payment is empty canceled'));
        return $this->render('payment_answer');
    }

}
